;; build-html.el --- script that builds HTML documentation from my Emacs configuration.

;;; Commentary:

;; Copyright (C) 2023 Gleb Smirnov <glebsmirnov0708@gmail.com>

;; Author: Gleb Smirnov

;;; Code:

;; Initialize package system
(require 'package)

(setq package-user-dir (expand-file-name "./packages")
      package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)

(unless package-archive-contents
  (package-refresh-contents))

;; Install `doom-themes' and `htmlize'

(package-install 'htmlize)
(package-install 'doom-themes)

;; Generate document CSS

(require 'color)

(defun normalize-color (color)
  "Convert color string COLOR to normalized 2-digit hex."
  (apply #'color-rgb-to-hex (append (color-name-to-rgb color) '(2))))

(require 'seq)
(require 'faces)
(require 'org)

(defun generate-theme-css ()
  "Generate theme-specific CSS for HTML export of `org-mode' file."
  (let ((syntax-styles
         (seq-filter (lambda (x) x)
                     (mapcar
                      (lambda (face)
                        (message (symbol-name face))
                        (pcase (symbol-name face)
                          ((rx "font-lock-" (let style (one-or-more (any letter "-"))) "-face")
                           (when-let (color (face-foreground face nil '(default)))
                             (format ".org-%s { color: %s; }\n"
                                     style
                                     (normalize-color color))))
                          (_ nil)))
                      (face-list)))))
    (format
     "body { color: %s; background-color: %s; }
pre.src { background-color: %s; }
a { color: %s; }
a:visited { color: %s; }
%s"
     (normalize-color (face-foreground 'default))
     (normalize-color (face-background 'default))
     (normalize-color (face-background 'org-inline-src-block nil '(default)))
     (normalize-color (face-foreground 'link nil '(default)))
     (normalize-color (face-foreground 'link-visited nil '(default)))
     (apply #'concat syntax-styles))))

;; Common CSS
(defvar common-css "body {
  font-family: 'JetBrains Mono';
  font-weight: normal;
  width: calc(min(700px, 90%));
  margin: auto;
}

pre.src {
  font-family: 'JetBrains Mono';
  font-size: 10pt;
  padding: 8px;
  border-radius: 5px;
  overflow: auto;
  border-width: 1px;
  border-style: solid;
  margin-bottom: 20px;
}

h2 {
  margin-top: 50px;
}

h3 {
  margin-top: 40px;
}

h4 {
  margin-top: 30px;
}")

;; Generate light theme CSS
(load-theme 'doom-one-light t)
(defvar light-css (generate-theme-css))

;; Generate dark theme CSS
(load-theme 'doom-dracula t)
(defvar dark-css (generate-theme-css))

;; Join CSS together
(defvar css-preset
  (format "%s
@media(prefers-color-scheme: light) {
pre.src { border-color: #dddddd; }
%s
}

@media(prefers-color-scheme: dark) {
pre.src { border-color: #222222; }
%s
}" common-css light-css dark-css))

(require 'ox-html)
(require 'htmlize)

;; Use custom CSS stylesheet for exported document
(setq org-html-head-include-default-style nil
      org-export-with-toc t
      org-export-with-section-numbers nil
      org-html-postamble nil
      org-html-htmlize-output-type 'css
      org-export-headline-levels 3
      org-html-head (format "
<style>%s</style>
<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=JetBrains Mono\">"
                            css-preset))

(with-current-buffer (find-file-noselect (car command-line-args-left))
  (org-html-export-to-html))


(kill-emacs 0)
(provide 'build-html)
;;; build-html.el ends here.
